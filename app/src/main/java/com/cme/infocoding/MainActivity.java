package com.cme.infocoding;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.cme.infocoding.utils.CodingUtils;
import com.cme.infocoding.utils.TableBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class MainActivity extends ActionBarActivity {

    private EditText mInputSequence;
    private Button mBtnSourceCoding;
    private Button mBtnBuildTable;
    private TextView mEntropy;
    private String entropyStr;

    private ArrayList<String> symbols;
    private ArrayList<Integer> occurrences;
    private List<Float> probability;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEntropy = (TextView) findViewById(R.id.entropy_text);
        mInputSequence = (EditText) findViewById(R.id.edit_input_text);
        mBtnSourceCoding = (Button) findViewById(R.id.btn_source_coding);

        mBtnSourceCoding.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String[] sourceCodingNames = new String[]{Constant.HUFFMAN, Constant.SHANON_FANO};
                ListAdapter adapter = new ArrayAdapter<String>(MainActivity.this,
                        android.R.layout.select_dialog_item, android.R.id.text1, sourceCodingNames);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Choose the source coding")
                        .setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.putStringArrayListExtra(Constant.SYMBOLS_BUNDLE_KEY, symbols);
                                intent.putIntegerArrayListExtra(Constant.OCCURRENCES_BUNDLE_KEY, occurrences);
                                intent.putExtra(Constant.ENTROPY_BUNDLE_KEY, entropyStr);
                                intent.putExtra(Constant.INPUT_TEXT_BUNDLE_KEY, mInputSequence.getText().toString());
                                intent.putExtra(Constant.SOURCE_CODING_BUNDLE_KEY, sourceCodingNames[which]);
                                intent.setClass(MainActivity.this, SourceCodingActivity.class);
                                startActivityForResult(intent, 0);

                            }
                        });
                builder.show();

            }
        });

        mBtnBuildTable = (Button) findViewById(R.id.btn_build_table);
        mBtnBuildTable.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                /*in case of empty input text*/
                if (mInputSequence.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Please enter some text ...", Toast.LENGTH_SHORT).show();
                } else {
                    TableBuilder probabilityTable = new TableBuilder(
                            mInputSequence.getText().toString());
                    probabilityTable.build();
                    probability = new ArrayList<Float>();
                    symbols = new ArrayList<String>();
                    occurrences = new ArrayList<Integer>();
                    symbols.addAll(probabilityTable.getSymbol());
                    occurrences.addAll(probabilityTable.getOccurrence());

                    mBtnSourceCoding.setEnabled(true);
                    createTableRow(v, probabilityTable);
                    entropyStr = Float.toString(CodingUtils.calculateEntropy(probability));
                    mEntropy.setText(Html.fromHtml("<b>Entropy: </b>" + entropyStr));
                }

            }
        });

    }

    /**
     * will create table automatically based on the input text
     *
     * @param v the view
     * @param t TableBuilder object
     */
    public void createTableRow(View v, TableBuilder t) {
        TableLayout tl = (TableLayout) findViewById(R.id.probability_table);
        probability = CodingUtils.calculateProbabilities(occurrences, t.getInput().length());

        /*just to delete table old values except for the titles that's why we start from 1*/
        int count = tl.getChildCount();
        for (int i = 1; i < count; i++) {
            View child = tl.getChildAt(i);
            if (child instanceof TableRow) ((ViewGroup) child).removeAllViews();
        }

        List<String> symbols = t.getSymbol();
        LayoutParams lp = new LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        LayoutParams llp = new LayoutParams(
                100, LayoutParams.WRAP_CONTENT, 1f);
        llp.setMargins(4, 4, 4, 4);

        for (int i = 0; i < symbols.size(); i++) {
            TableRow tr = new TableRow(this);
            tr.setPadding(10, 0, 5, 0);
            tr.setLayoutParams(lp);

            TextView tvLeft = new TextView(this);
            tvLeft.setLayoutParams(llp);
            tvLeft.setPadding(5, 5, 5, 5);
            tvLeft.setBackgroundResource(R.drawable.shape);
            tvLeft.setGravity(Gravity.CENTER);
            tvLeft.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            tvLeft.setText(symbols.get(i));
            TextView tvRight = new TextView(this);
            tvRight.setPadding(5, 5, 5, 5);
            tvRight.setBackgroundResource(R.drawable.shape);
            tvRight.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            tvRight.setText(probability.get(i).toString());
            tvRight.setLayoutParams(llp);
            tvRight.setGravity(Gravity.CENTER);
            tr.addView(tvLeft);
            tr.addView(tvRight);
            tl.addView(tr);
        }

    }


}
