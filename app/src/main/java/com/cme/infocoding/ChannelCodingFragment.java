package com.cme.infocoding;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;


/**
 * A placeholder fragment containing a simple view.
 */
public class ChannelCodingFragment extends Fragment {

    protected String bitSequenceString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bitSequenceString= getArguments().getString(Constant.BIT_SEQUENCE_BUNDLE_KEY);
        View rootView = inflater.inflate(R.layout.fragment_channel_coding, container, false);
        TextView tv = (TextView) rootView.findViewById(R.id.bit_sequence_text);
        tv.setText(bitSequenceString);


        return rootView;
    }
}
