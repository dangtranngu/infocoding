package com.cme.infocoding.codings.sourcecodings;

import android.util.Log;

import com.cme.infocoding.utils.TableBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

/**
 * Huffman source coding
 * @author Hakeem
 *
 */
public class Huffman extends SourceCoding{
    private List<String> symbol = new ArrayList<>();
    private List<Integer> table = new ArrayList<Integer>();
    private List<String> code = new ArrayList<>();
    private List<String> tCode = new ArrayList<>();
    private List<String> indexList = new ArrayList<>();
    private List<Integer> pTable = new ArrayList<>();
    private Map<String, String> codingTable = new HashMap<>();
    private Map<String, String> decodingTable = new HashMap<>();
    private ListIterator<String> printIterator;
    private List<String> occurences= new ArrayList<>();
    private List<String> indexs = new ArrayList<>();
    private String stringInput;
    private final String TAG ="Huffman";
    private String  steps = String.format("%16s%32s","Occurencs","indexs ")+"\n";



    /**
     * the constructor that used for textinput
     * @param symbols
     * @param occurrences
     * @param userInput
     */
    public Huffman(ArrayList<String> symbols, ArrayList<Integer> occurrences , String userInput) {


        for (int i = 0; i < occurrences.size(); i++) {
            table.add(occurrences.get(i));
            String empty = "";
            tCode.add(empty);
            // space here created to prevent wrong coding of the index E.g: 10
            // as index is "10" not "1"+"0"
            indexList.add(Integer.toString(i) + " ");
            pTable.add(occurrences.get(i));
            symbol.add(symbols.get(i));
        }
        this.stringInput = userInput;

    }


    @Override
    public void preparation() {


            printIterator = tCode.listIterator();
            buildTable(table, indexList);

            Log.i(TAG,"\n SOURCE CODING :HUFFMAN CODING \n");

                int j = 0;
                while (printIterator.hasNext()) {
                    String r = new StringBuilder(printIterator.next())
                            .reverse().toString();
                    code.add(r);
                    codingTable.put(symbol.get(j), r);
                    decodingTable.put(r, symbol.get(j));
                    Log.i(TAG,symbol.get(j) + " - "
                            + pTable.get(printIterator.previousIndex()) + " - "
                            + r);
                    j++;
                }
            }

    /**
     *
     * @param table
     * @param indList
     */

    private void buildTable(List<Integer> table, List<String> indList) {
        int s = table.size();
        int sumOfLast2 = 0;
        if(s==1){
            bitCoding(indList.get(s - 1), (byte) 1);
        }else{
            bitCoding(indList.get(s - 1), (byte) 1);
            bitCoding(indList.get(s - 2), (byte) 0);
        }


        for (int i = 0; i < table.size(); i++) {
            Log.i(TAG,table.get(i) + " ---- " + indList.get(i));
            steps += String.format("%16s%32s", table.get(i).toString(), indList.get(i))+"\n";
            occurences.add(table.get(i).toString());
            indexs.add(indList.get(i));
            /*steps += table.get(i) + " ---- " + indList.get(i) + "\n";*/

        }
        occurences.add("=");
        indexs.add("=");
        steps += String.format("%48s","===========================")+"\n";
        Log.i(TAG,"===============");


        if ((s == 2) | (s == 1))
            return;

        sumOfLast2 = table.get(s - 1) + table.get(s - 2);
        table.remove(s - 1);
        table.remove(s - 2);
        String temp = indList.get(s - 1) + indList.get(s - 2);
        indList.remove(s - 1);
        indList.remove(s - 2);

        if (sumOfLast2 > table.get(0)) {
            table.add(0, sumOfLast2);
            indList.add(0, temp);
        } else {
            for (int g = s - 3; g >= 0; g--) {
                if (sumOfLast2 <= table.get(g)) {
                    table.add(g + 1, sumOfLast2);
                    indList.add(g + 1, temp);
                    break;
                }

            }
        }

        buildTable(table, indList);

    }



    /**
     *
     * @param index1
     * @param g
     */
    private void bitCoding(String index1, byte g) {
        String indexChar = "";
        for (int i = 0; i < index1.length(); i++) {

            int tempIndex = index1.charAt(i);

            if (tempIndex == 32) {
                // to remove the space that we create earlier, 32 in ACII is " "
                int index = Integer.parseInt(indexChar);
                tCode.set(index, tCode.get(index) + g);
                indexChar = "";
            } else {
                indexChar += index1.charAt(i);
            }

        }

    }

    /**
     * @return codedBlock : binary output of the input text based on the
     *         coding table created earlier.
     */
    public String inputEncoding() {
        String s = stringInput;
        String codedBlock = "";
        Set<String> k = codingTable.keySet();
        for (int i = 0; i < s.length(); i++) {
            String d = "" + s.charAt(i);
            if (k.contains(d)) {
                codedBlock += codingTable.get(d);
            }
        }

        return codedBlock;

    }

    /**
     * @param  input  codedBlock in form of 0 & 1 .
     * @return String decodedBlock : translation of that binary bit sequence to
     *         its original form.
     */
    public String inputDecoding(String input) {
        String decodedBlock = "";
        String buffer = "";
        Set<String> k = decodingTable.keySet();
        for (int i = 0; i < input.length(); i++) {
            buffer += input.charAt(i);
            if (k.contains(buffer)) {
                decodedBlock += decodingTable.get(buffer);
                buffer = "";
            }

        }

        return decodedBlock;

    }

    public List<String> getSymbol() {
        return symbol;
    }
    public Map<String, String> getCodingTable() {
        return codingTable;
    }
    public String getSteps() {
        return steps;
    }

    public List<Integer> getpTable() {
        return pTable;
    }

    public List<String> getCode() {
        return code;
    }

    public List<String> getOccurences() {
        return occurences;
    }

    public List<String> getIndexs() {
        return indexs;
    }
}
