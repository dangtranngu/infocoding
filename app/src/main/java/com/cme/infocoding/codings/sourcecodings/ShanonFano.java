package com.cme.infocoding.codings.sourcecodings;


import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * SHANON FANO SOURCE CODING
 *
 * @author Hakeem
 */
public class ShanonFano extends SourceCoding {

    private List<Integer> table = new ArrayList<>();
    private List<String> code = new ArrayList<>();
    private List<String> symbol = new ArrayList<>();
    private List<Integer> indexList = new ArrayList<>();
    private List<Integer> pTable = new ArrayList<>();
    private Map<String, String> codingTable = new HashMap<>();
    private Map<String, String> decodingTable = new HashMap<>();
    private String stringInput;
    private List<String> occurrences = new ArrayList<>();
    private List<String> codingProcess = new ArrayList<>();
    private final String TAG = "ShanonFano";


    /**
     * @param symbols
     * @param occurrences
     * @param userInput
     */
    public ShanonFano(ArrayList<String> symbols, ArrayList<Integer> occurrences, String userInput) {

        Collections.sort(occurrences);
        for (int i = 0; i < occurrences.size(); i++) {
            table.add(occurrences.get(i));
            code.add("");// creating empty Code table to be able to use set
            indexList.add(i);
            pTable.add(occurrences.get(i));
            symbol.add(symbols.get(i));
        }
        this.stringInput = userInput;
    }

    /**
     *
     */
    @Override
    public void preparation() {
        buildTable(table, indexList);
        Log.i(TAG, "\n SOURCE CODING: SHANON CODING \n");
        int j = 0;
        for (int i = pTable.size() - 1; i >= 0; i--) {
            Log.i(TAG, symbol.get(j) + " -- " + pTable.get(i)
                    + " -- " + code.get(i));
            codingTable.put(symbol.get(j), code.get(i));
            decodingTable.put(code.get(i), symbol.get(j));
            j++;

        }
    }


    /**
     * @param frequency
     * @param indList
     */
    private void buildTable(List<Integer> frequency, List<Integer> indList) {
        int tSum = 0;
        for (int i : frequency) {// calculating the sum everytime
            tSum += i;
        }
        int s = frequency.size();
        int halfPoint = (s == 1) ? tSum : tSum / 2;
        int pointer = splitPoint(frequency, halfPoint);
        if (frequency.get(s - 1) == pointer) {
            bitCoding(indList.get(s - 1), indList);
            indList.remove(s - 1);
            frequency.remove(s - 1);
        } else {
            List<Integer> indexList1 = new ArrayList<>();
            List<Integer> frequency1 = new ArrayList<>();
            int tsum = 0;
            // Sum all the numbers up to the pointer
            for (int i = s - 1; i >= 0; i--) {
                tsum += frequency.get(i);
                if (tsum == pointer) {
                    bitCoding(indList.get(i), indList);
                    for (int g = s - 1; g >= i; g--) {
                        indexList1.add(indList.get(g));
                        frequency1.add(frequency.get(g));
                        indList.remove(g);
                        frequency.remove(g);
                    }
                    break;
                }
            }
            Collections.sort(indexList1);
            Collections.sort(frequency1);
            buildTable(frequency1, indexList1);
        }
        if ((s == 2) | (s == 1))
            /* in some cases the number of grouping could be odd
             so we might reach s=1 E.g :seven 5's the sum is
			 35 and last group is three 5's together and one 5
			 left*/
            return;
        buildTable(frequency, indList);
    }

    /**
     * @param indexP    the last index in the group
     * @param indexList indexList before removing the group of indexes which sum =
     *                  Split pointer
     */
    private void bitCoding(int indexP, List<Integer> indexList) {
        // Putting zero above the line
        for (int g = indexP; g <= indexList.get(indexList.size() - 1); g++) {
            code.set(g, code.get(g) + "0");
        }
        // putting one under the line
        for (int i = indexList.get(0); i < indexP; i++) {
            code.set(i, code.get(i) + "1");
        }
        int h = 0;
        for (int i = symbol.size() - 1; i >= 0; i--) {
            Log.i(TAG, symbol.get(h) + "---" + pTable.get(i) + "----" + code.get(i));
            occurrences.add(pTable.get(i).toString());
            codingProcess.add(code.get(i));
            h++;
        }
        Log.i(TAG, "=================");
        occurrences.add("=");
        codingProcess.add("=");
    }

    /**
     * this function will determine where we should put the line Smart Decision
     * Mechanism
     *
     * @param frequency List  of probabilities
     * @param halfPoint
     * @return
     */

    private int splitPoint(List<Integer> frequency, int halfPoint) {
        int sum = 0;
        int previousSum = 0;
        int F = frequency.size() - 1;
        int r = 0;

        for (int i = F; i >= 0; i--) {
            sum += frequency.get(i);
            if (sum == halfPoint) {

                r = sum;
                break;
            } else if (sum > halfPoint) {
                int x = sum - halfPoint;
                int y = halfPoint - previousSum;
                if (y < x) {
                    r = previousSum;
                } else {
                    r = sum;
                }
                break;
            }

            previousSum = sum;
        }
        return r;

    }

    /**
     * @return codedWord : coded binary word of the input text based on the
     * coding table created earlier.
     */
    public String inputEncoding() {
        String s = stringInput;
        String codedBlock = "";
        Set<String> k = codingTable.keySet();
        for (int i = 0; i < s.length(); i++) {
            String d = "" + s.charAt(i);
            if (k.contains(d)) {
                codedBlock += codingTable.get(d);
            }
        }


        return codedBlock;

    }

    /**
     * @param input in form of 0 & 1 .
     * @return String decodedWord : translation of that binary bit sequence to
     * its original form.
     */
    public String inputDecoding(String input) {
        String decodedBlock = "";
        String buffer = "";
        Set<String> k = decodingTable.keySet();
        for (int i = 0; i < input.length(); i++) {
            buffer += input.charAt(i);
            if (k.contains(buffer)) {
                decodedBlock += decodingTable.get(buffer);
                buffer = "";
            }

        }


        return decodedBlock;

    }

    public List<String> getCode() {
        return code;
    }

    public Map<String, String> getCodingTable() {
        return codingTable;
    }

    public List<String> getSymbol() {
        return symbol;
    }

    public List<Integer> getpTable() {
        return pTable;
    }

    public List<String> getOccurrences() {
        return occurrences;
    }

    public List<String> getCodingProcess() {
        return codingProcess;
    }
}




