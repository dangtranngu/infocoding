package com.cme.infocoding.codings.channelcodings;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Rectangular  channel coding
 * @author Hakeem
 *
 */
public class Rectangular {
    private	List<String> table = new ArrayList<>();
    // this table is the same as original table including the parity bits
    private	List<String> parityTable = new ArrayList<>();
    private final String TAG = "Rectangular";
    /**
     *
     * @param rect
     */
    public Rectangular(List<String>rect) {
        boolean a = true;
        int size = rect.size();
        // Checking the length of the String input sequence
        for (int i = 0; i < size; i++) {
            if (rect.get(0).length() != rect.get(i).length()) {
                Log.e(TAG, "Same word length required !!");
                a = false;
                break;
            }
        }
        if (a) {
            this.table = rect;

            for (String i : table) {
                parityTable.add(i);
            }
        }

    }

    /**
     *
     */
    public void doRectangle() {
        rightParity(table);// adding even parity bit to each row
        downParity(table);// adding even parity bit to each column
        Log.i(TAG,"\n CHANNEL CODING : RECTANGLE \n");
        for(String s: parityTable){
            Log.i(TAG,s);
        }
    }

    /**
     *
     * @param table
     */
    private void rightParity(List<String> table) {
        for (int i = 0; i < table.size(); i++) {
            // row navigation
            int counter = 0;
            for (int g = 0; g < table.get(i).length(); g++) {
                // column navigation
                if (table.get(i).charAt(g) == 49) {
                    // counting ones ACII 49 is '1'
                    counter++;
                }

            }
            if (counter % 2 == 0) {
                // if the number of ones is even put zero else put 1 "even parity"
                parityTable.set(i, parityTable.get(i) + "0");
            } else {
                parityTable.set(i,parityTable.get(i) + "1");
            }
        }

    }

    /**
     *
     * @param table
     */
    private void downParity(List<String> table) {
        parityTable.add("");
        for (int i = 0; i < table.get(0).length(); i++) {
            // column navigation
            int counter = 0;
            for (int g = 0; g < table.size(); g++) {
                //row navigation
                if (table.get(g).charAt(i) == 49) {
                    // counting ones ACII 49 is'1'
                    counter++;
                }

            }
            if (counter % 2 == 0) {
                //if the number of ones is even add "1" or add "0" , even parity
                parityTable.set(parityTable.size() - 1,
                        parityTable.get(parityTable.size() - 1) + "0");
            } else {
                parityTable.set(parityTable.size() - 1,
                        parityTable.get(parityTable.size() - 1) + "1");
            }

        }

    }

    public List<String> getTable() {
        return table;
    }

    public List<String> getParityTable() {
        return parityTable;
    }

}
