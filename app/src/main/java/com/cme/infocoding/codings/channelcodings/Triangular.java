package com.cme.infocoding.codings.channelcodings;

import android.util.Log;

import java.util.List;


import java.util.ArrayList;
import java.util.List;
/**
 * triangle channel coding
 * @author Hakeem
 *
 */
public class Triangular {

    private List<String> table = new ArrayList<>();
    private List<String> parityTable = new ArrayList<>();
    private final String TAG = "Triangular";
    /**
     * @param tri
     */
    public Triangular(List<String> tri) {
        if (tri.get(0).length()==1) {

            table = tri;
            parityTable = tri;
        } else {
            Log.e(TAG, "triangle Build Required!!");
        }

    }

    public void doTriangle() {
        int firstCounter = 0;
        parityTable.add(0, "");
        for (int first = 1; first < table.size(); first++) {
            // for the first column

            if (table.get(first).charAt(0) == 49) {
                firstCounter++;
            }
        }

        if (firstCounter % 2 == 0) {
            parityTable.set(0, "0");
        } else {
            parityTable.set(0, "1");
        }
        for (int row = 1; row < table.size(); row++) {
            // for the rest starting from the second row ;
            int counter = 0;
            for (int col = 0; col < table.get(row).length(); col++) {
                //counting the number of 1's in each row
                if (table.get(row).charAt(col) == 49) {
                    counter++;
                }
                if (col == table.get(row).length() - 1) {
                    //once we finish counting 1's in one row
                    for (int cRow = row + 1; cRow < table.size(); cRow++) {
                        //counting the number of 1's in each row
                        if (table.get(cRow).charAt(col+1) == 49) {
                            counter++;
                        }

                    }
                }
            }

            if (counter % 2 == 0) {
                parityTable.set(row, parityTable.get(row) + "0");
            } else {
                parityTable.set(row, parityTable.get(row) + "1");
            }

        }
        Log.i(TAG,"\n CHANNEL CODING : TRIANGLE \n");
        for (String s : parityTable) {
            Log.i(TAG,s);
        }

    }



    public List<String> getTable() {
        return table;
    }

    public List<String> getParityTable() {
        return parityTable;
    }

}
