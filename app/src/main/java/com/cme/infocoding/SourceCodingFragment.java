package com.cme.infocoding;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A placeholder fragment containing a simple view.
 */
public abstract class SourceCodingFragment extends Fragment {

    protected TextView mInputText;
    protected TextView mCodeBlock;
    protected TextView mInfoRate;
    protected TextView mRedundancy;

    protected Button mBtnShowSteps;
    protected ArrayList<String> symbols;
    protected ArrayList<Integer> occurrences;
    protected String userInput ;
    protected float entropy;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_source_coding, container, false);

        symbols = getArguments().getStringArrayList(Constant.SYMBOLS_BUNDLE_KEY);
        occurrences = getArguments().getIntegerArrayList(Constant.OCCURRENCES_BUNDLE_KEY);
        userInput= getArguments().getString(Constant.INPUT_TEXT_BUNDLE_KEY);
        entropy = Float.parseFloat(getArguments().getString(Constant.ENTROPY_BUNDLE_KEY));

        mInputText = (TextView) rootView.findViewById(R.id.input_text);
        mInputText.setText(userInput);

        mInfoRate = (TextView) rootView.findViewById(R.id.info_rate_text);
        mRedundancy = (TextView) rootView.findViewById(R.id.redundancy_text);
        mCodeBlock = (TextView) rootView.findViewById(R.id.code_block);

        Button btnChannelCoding = (Button) rootView.findViewById(R.id.btn_channel_coding);
        btnChannelCoding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] channelCodingNames = new String[]{Constant.RECTANGLE, Constant.TRIANGLE};
                ListAdapter adapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.select_dialog_item, android.R.id.text1, channelCodingNames);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose the channel coding")
                        .setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.putExtra(Constant.CHANNEL_CODING_BUNDLE_KEY, channelCodingNames[which]);
                                intent.putExtra(Constant.BIT_SEQUENCE_BUNDLE_KEY, mCodeBlock.getText());
                                intent.setClass(getActivity(), ChannelCodingActivity.class);
                                startActivityForResult(intent, 0);
                            }
                        });
                builder.show();

            }
        });
        return rootView;
    }

    protected abstract class ShowStepAsync extends AsyncTask<Void,Void,Void> {

        ProgressDialog mProgressDialog;
        protected Dialog mShowStepsDialog;
        protected TableLayout mTableLayout;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Preparing steps ...");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    ShowStepAsync.this.cancel(true);
                }
            });
            mProgressDialog.show();

            //tell the Dialog to use the dialog.xml as it's layout description
            mShowStepsDialog = new Dialog(getActivity());
            mShowStepsDialog.setContentView(R.layout.custom_alert_dialog);
            mShowStepsDialog.setTitle("Steps");
            mTableLayout = (TableLayout) mShowStepsDialog.findViewById(R.id.show_steps_table);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            mShowStepsDialog.show();
        }

    }

}
