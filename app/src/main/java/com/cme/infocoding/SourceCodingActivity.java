package com.cme.infocoding;


import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.TreeMap;


public class SourceCodingActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_source_coding);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        String srcCodingName = getIntent().getStringExtra(Constant.SOURCE_CODING_BUNDLE_KEY);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(srcCodingName);

        ArrayList<String> symbols = (ArrayList<String>)getIntent()
                .getStringArrayListExtra(Constant.SYMBOLS_BUNDLE_KEY);
        ArrayList<Integer> occurrences = (ArrayList<Integer>)getIntent()
                .getIntegerArrayListExtra(Constant.OCCURRENCES_BUNDLE_KEY);
        String userInput = getIntent().getStringExtra(Constant.INPUT_TEXT_BUNDLE_KEY);
        String entropy = getIntent().getStringExtra(Constant.ENTROPY_BUNDLE_KEY);

        final SourceCodingFragment fragment;
        if (srcCodingName.equals(Constant.HUFFMAN))
            fragment = new SourceCodingHuffmanFragment();
        else {
            fragment = new SourceCodingShanonFanoFragment();
        }

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constant.SYMBOLS_BUNDLE_KEY, symbols);
        bundle.putIntegerArrayList(Constant.OCCURRENCES_BUNDLE_KEY, occurrences);
        bundle.putString(Constant.INPUT_TEXT_BUNDLE_KEY, userInput);
        bundle.putString(Constant.ENTROPY_BUNDLE_KEY, entropy);
        fragment.setArguments(bundle);

        fragmentTransaction.add(R.id.source_coding_fragment, fragment);
        fragmentTransaction.commit();


    }


}
