package com.cme.infocoding;

/**
 * Created by ngu on 20.06.15.
 */
public class Constant {
    public static String HUFFMAN = "Huffman";
    public static String SHANON_FANO = "ShanonFano";
    public static String SOURCE_CODING_BUNDLE_KEY = "source_coding";
    public static String INPUT_TEXT_BUNDLE_KEY = "input_text";
    public static String SYMBOLS_BUNDLE_KEY = "symbols";
    public static String OCCURRENCES_BUNDLE_KEY = "occurrences";

    public static String RECTANGLE = "Rectangle";
    public static String TRIANGLE = "Triangle";
    public static String CHANNEL_CODING_BUNDLE_KEY = "channel_coding";
    public static String BIT_SEQUENCE_BUNDLE_KEY = "bit_sequence";
    public static String ENTROPY_BUNDLE_KEY = "entropy";
}
