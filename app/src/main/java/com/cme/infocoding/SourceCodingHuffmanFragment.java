package com.cme.infocoding;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.cme.infocoding.codings.sourcecodings.Huffman;
import com.cme.infocoding.utils.CodingUtils;


/**
 * A placeholder fragment containing a simple view.
 */
public class SourceCodingHuffmanFragment extends SourceCodingFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        final Huffman h = new Huffman(symbols, occurrences, userInput);
        h.preparation();
        float infoRate = CodingUtils.calculateInfoRate(
                CodingUtils.calculateProbabilities(occurrences, userInput.length()),
                h.getCode());

        mInfoRate.setText(Html.fromHtml(
                "<b>Average Word Length: </b>" + Float.toString(infoRate)));
        mRedundancy.setText(Html.fromHtml(
                "<b>Redundancy: </b>" + CodingUtils.calculateRedundancy(entropy, infoRate)));

        mBtnShowSteps = (Button) rootView.findViewById(R.id.btn_show_steps);
        mBtnShowSteps.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                new ShowStepAsync() {

                    @Override
                    protected Void doInBackground(Void... voids) {
                        int c = 0;
                        TableRow.LayoutParams lp = new TableRow.LayoutParams(
                                TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                        TableRow.LayoutParams llp = new TableRow.LayoutParams(
                                100, TableRow.LayoutParams.WRAP_CONTENT, 1f);
                        llp.setMargins(4, 4, 4, 4);

                        for (String i : h.getOccurences()) {
                            TableRow tr = new TableRow(mShowStepsDialog.getContext());
                            tr.setPadding(10, 0, 5, 0);
                            tr.setLayoutParams(lp);

                            if (!i.equals("=")) {
                                TextView tvLeft = new TextView(mShowStepsDialog.getContext());
                                tvLeft.setLayoutParams(llp);
                                tvLeft.setPadding(5, 5, 5, 5);
                                tvLeft.setBackgroundResource(R.drawable.shape);
                                tvLeft.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                                tvLeft.setText(i);

                                TextView tvRight = new TextView(mShowStepsDialog.getContext());
                                tvRight.setPadding(5, 5, 5, 5);
                                tvRight.setBackgroundResource(R.drawable.shape);
                                tvRight.setText(h.getIndexs().get(c));
                                tvRight.setSingleLine();
                                tvRight.setEllipsize(TextUtils.TruncateAt.START);
                                tvRight.setLayoutParams(llp);
                                tvRight.setGravity(Gravity.CENTER);

                                tr.addView(tvLeft);
                                tr.addView(tvRight);
                            } else {
                                TextView tvLeft = new TextView(mShowStepsDialog.getContext());
                                tvLeft.setLayoutParams(llp);
                                tvLeft.setPadding(5, 5, 5, 5);
                                tvLeft.setBackgroundResource(R.drawable.shape);
                                tvLeft.setGravity(Gravity.CENTER);

                                tvLeft.setText("--- NEXT STEP ---");
                                tr.addView(tvLeft);
                            }
                            mTableLayout.addView(tr);
                            c++;
                        }

                        return null;
                    }

                }.execute();
            }

        });

        createTableRow(rootView, h);

        mCodeBlock.setText(h.inputEncoding());
        return rootView;
       }

    public void createTableRow(View v, Huffman huff) {
        TableLayout tl = (TableLayout) v.findViewById(R.id.coding_table);

        TableRow.LayoutParams lp = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);

        TableRow.LayoutParams llp = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        llp.setMargins(4, 4, 4, 4);

        for (int i = 0; i < huff.getSymbol().size(); i++) {

            TableRow tr = new TableRow(getActivity());
            tr.setPadding(10, 0, 5, 0);
            tr.setLayoutParams(lp);

            // Symbol column
            TextView tvSymbol = new TextView(getActivity());
            tvSymbol.setLayoutParams(llp);
            tvSymbol.setPadding(5, 5, 5, 5);
            tvSymbol.setBackgroundResource(R.drawable.shape);
            tvSymbol.setGravity(Gravity.CENTER);

            tvSymbol.setText(huff.getSymbol().get(i));

            // Occurences column
            TextView tvOccurences = new TextView(getActivity());
            tvOccurences.setLayoutParams(llp);
            tvOccurences.setPadding(5, 5, 5, 5);
            tvOccurences.setBackgroundResource(R.drawable.shape);
            tvOccurences.setGravity(Gravity.CENTER);

            tvOccurences.setText(huff.getpTable().get(i).toString());

            // Coding column
            TextView tvCoding = new TextView(getActivity());
            tvCoding.setLayoutParams(llp);
            tvCoding.setSingleLine();
            tvCoding.setEllipsize(TextUtils.TruncateAt.START);
            tvCoding.setPadding(5, 5, 5, 5);
            tvCoding.setBackgroundResource(R.drawable.shape);
            tvCoding.setGravity(Gravity.CENTER);

            tvCoding.setText(huff.getCode().get(i));

            tr.addView(tvSymbol);
            tr.addView(tvOccurences);
            tr.addView(tvCoding);
            tl.addView(tr);

        }
    }



}