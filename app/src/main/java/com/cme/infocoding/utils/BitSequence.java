package com.cme.infocoding.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;


/**
 * this class is used to make the bit sequence after applying huffman or
 * shanonfano in proper shape for applying rectangular or triangular channel
 * coding
 *
 * @author Hakeem
 *
 */
public class BitSequence {


    private List<String> rect = new ArrayList<>();
    private List<String> tri = new ArrayList<>();
    private String codeBlock = "";
    private final String TAG="BitSequence";

    /**
     * @param codeBlock
     *            output of source coding
     */
    public BitSequence(String codeBlock) {
        this.codeBlock = codeBlock;

    }

    /**
     * reshape the result source coding table in a rectangle shape
     */
    public void rectBitStream() {
        int numberOfBits = codeBlock.length();// number of bits in
        String sequence = codeBlock;
        int row = (int) Math.sqrt(numberOfBits);
        int column = 0;
        for (int i = 1; i <= numberOfBits; i++) {
            int v = row * i;
            if (v >= numberOfBits) {
                column = i;

                break;
            }
        }

        int area = row * column;
        if (area != numberOfBits) {
            int C = area - numberOfBits;
            for (int b = 0; b < C; b++) {
                sequence += "0";

            }
        }
        String buffer = "";
        for (int i = 0; i < sequence.length(); i++) {
            buffer += sequence.charAt(i);
            if (buffer.length() == column) {
                rect.add(buffer);
                buffer = "";

            }
        }

        for (String V : rect) {

            Log.i(TAG, V);
        }
        ;

    }

    /**
     * reshape the result source coding table in triangular shape
     */
    public void triBitStream() {

        int numberOfBits = codeBlock.length();// number of bits in
        // total
        String sequence = codeBlock;
        if (sequence.length() == 1) {
            tri.add(sequence);
        } else {
            int x = (int) Math.sqrt(2 * numberOfBits);
            while ((x * (x + 1)) < 2 * numberOfBits) {
                x = x + 1;
            }
            int totalNumber = x * (x + 1) / 2;
            while (totalNumber > numberOfBits) {
                totalNumber--;
                sequence += "0";

            }

            String tempS = sequence;
            for (int i = 1; i < sequence.length(); i++) {

                if (i >= tempS.length() - 1) {
                    tri.add(tempS);
                    break;
                } else {

                    tri.add(tempS.substring(0, i));

                    tempS = tempS.substring(i);

                }
            }
        }

        for (String V : tri) {
          Log.i(TAG,V);
        }


    }

    public List<String> getRect() {
        return rect;
    }

    public List<String> getTri() {
        return tri;
    }


}
