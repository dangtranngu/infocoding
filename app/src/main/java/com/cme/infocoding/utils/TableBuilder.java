package com.cme.infocoding.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by Hakeem on 6/6/2015.
 */
public class TableBuilder {
    private List<String>symbol = new ArrayList<String>();
    private List<Integer> occurrence= new ArrayList<>();
    private String input;
    private Map<String, Integer> table = new HashMap<String, Integer>();
    private ValueComparator bvc = new ValueComparator(table);
    // for ordering purpose only
    private TreeMap<String, Integer> sortedTable = new TreeMap<>(bvc);
    private static final String TAG = "TableBuilder";
    /**
     *
     * @param input : text input
     */
    public TableBuilder(String input) {
        if(!input.equals("")){
            this.input = input;
        }else{
            Log.e(TAG,"Empty!!");
        }
    }
    /**
     * build a table of symbols with their number of occurrence  in the text (descending order).
     */
    public void build() {
        for (int i = 0; i < input.length(); i++) {

            int counter = 0;
            for (int h = 0; h < input.length(); h++) {

                if (input.charAt(h) == input.charAt(i)) {
                    counter++;
                }
            }

            table.put(""+input.charAt(i), counter);

        }

        sortedTable.putAll(table);

        Set<String> a = sortedTable.keySet();

        for (String t : a) {
            symbol.add(t);
            occurrence.add(table.get(t));
            Log.i(TAG,t + " Ocurred " + table.get(t) + " times");
        }

    }
    /**
     *
     *
     *
     */
    class ValueComparator implements Comparator<String> {

        Map<String, Integer> base;

        public ValueComparator(Map<String, Integer> base) {
            this.base = base;
        }

        // Note: this comparator imposes orderings that are inconsistent with
        // equals.
        public int compare(String a, String b) {

            int r = base.get(a).compareTo(base.get(b));
            if(r >= 0){
                return -1;
            }else{
                return 1;
            }

        }
    }


    public List<String> getSymbol() {
        return symbol;
    }

    public List<Integer> getOccurrence() {
        return occurrence;
    }

    public Map<String, Integer> getTable() {
        return table;
    }

    public TreeMap<String, Integer> getSortedTable() {
        return sortedTable;
    }
    public String getInput() {
        return input;
    }
}
