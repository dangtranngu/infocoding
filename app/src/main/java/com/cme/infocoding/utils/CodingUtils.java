package com.cme.infocoding.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ngu on 24.05.15.
 */
public class CodingUtils {

    public static float calculateEntropy(List<Float> probabilities) {
        float entropy = 0;
        for (Float i : probabilities) {
            entropy += ((Math.log(1 / i) )/Math.log(2) )* i;
        }
        return entropy;
    }

    public static float calculateInfoRate(List<Float> probabilities, List<String> codeWords) {
        float informationRate = 0;
        for (int i = 0; i < probabilities.size(); i++) {
            informationRate += codeWords.get(i).length() * probabilities.get(i);
        }
        return informationRate;
    }

    public static float calculateRedundancy(float entropy, float informationRatio) {
        float redundancy = 0;
        redundancy = informationRatio - entropy;

        return redundancy;
    }
    
    public static List<Float> calculateProbabilities(List<Integer> occurrences , int length) {
        List<Float> prob = new ArrayList<Float>();
        for (int i=0;i<occurrences.size();i++){
            float p = (float)occurrences.get(i);
            prob.add(p/((float)length));

        }

        return prob;
    }

}
