package com.cme.infocoding;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;


public class ChannelCodingActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_coding);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        String channelCodingName = getIntent()
                .getStringExtra(Constant.CHANNEL_CODING_BUNDLE_KEY);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(channelCodingName);

        final ChannelCodingFragment fragment;
        if (channelCodingName.equals(Constant.TRIANGLE))
            fragment = new ChannelCodingTriangularFragment();
        else {
            fragment = new ChannelCodingRectangularFragment();
        }

        Bundle bundle = new Bundle();
        String bitSequenceString = (String)getIntent().getStringExtra(
                Constant.BIT_SEQUENCE_BUNDLE_KEY);
        bundle.putString(Constant.BIT_SEQUENCE_BUNDLE_KEY, bitSequenceString);
        fragment.setArguments(bundle);

        fragmentTransaction.add(R.id.channel_coding_fragment, fragment);
        fragmentTransaction.commit();
    }

}
