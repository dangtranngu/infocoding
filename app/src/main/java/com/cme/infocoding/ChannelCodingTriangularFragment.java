package com.cme.infocoding;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.cme.infocoding.codings.channelcodings.Rectangular;
import com.cme.infocoding.codings.channelcodings.Triangular;
import com.cme.infocoding.codings.sourcecodings.Huffman;
import com.cme.infocoding.utils.BitSequence;


/**
 * A placeholder fragment containing a simple view.
 */
public class ChannelCodingTriangularFragment extends ChannelCodingFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Initializing the constructor. This is the first thing you do when you create the onCreateView
        View rootView = super.onCreateView(inflater, container, savedInstanceState);


        BitSequence bitSequence = new BitSequence(bitSequenceString);
        bitSequence.triBitStream();
        Triangular triCoding = new Triangular(bitSequence.getTri());
        triCoding.doTriangle();

        String table = "";

        for (int i = 0; i < triCoding.getParityTable().size(); i++) {
            String s = triCoding.getParityTable().get(i);

            table += s.substring(0, s.length() - 1)
                    + "<font color='red'>" + s.substring(s.length() - 1) + "</font><br>";
        }


        TextView tv = (TextView) rootView.findViewById(R.id.parity_table);
        tv.setMovementMethod(new ScrollingMovementMethod());
        tv.setText(Html.fromHtml(table));

        TextView tv2 = (TextView) rootView.findViewById(R.id.output_bit_sequence_text);
        tv2.setMovementMethod(new ScrollingMovementMethod());
        tv2.setText(Html.fromHtml(table.replaceAll("<br>", "")));

        return rootView;
    }


}